<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\GuidelineController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\BackendUserController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\TypeController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UserDetailController;
use App\Http\Controllers\PregnantCodeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

// Route::get('login', 'App\Http\Controllers\Auth\LoginController@showLoginForm')->name('login');
// Route::post('login', 'App\Http\Controllers\Auth\LoginController@login');
// Route::post('logout', 'App\Http\Controllers\Auth\LoginController@logout')->name('logout');

// // Registration Routes...
// Route::get('register', 'App\Http\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
// Route::post('register', 'App\Http\Controllers\Auth\RegisterController@register');
// //Password Routes
// Route::get('password/reset', 'App\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
// Route::post('password/email', 'App\Http\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}', 'App\Http\Controllers\Auth\ResetPasswordController@showResetForm');
// Route::post('password/reset', 'App\Http\Controllers\Auth\ResetPasswordController@reset');
Auth::routes();


Route::middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/user/list', [UserController::class, 'list'])->name('user.list');
    Route::get('setting', [SettingController::class, 'index'])->name('setting.index');
    Route::post('updateUserStatus/{id}',[UserDetailController::class, 'updateUserStatus'])->name('user.updateUserStatus');
    Route::get('updateBackendUserStatus/{id}',[UserController::class, 'updateBackendUserStatus'])->name('user.updateBackendUserStatus');
    Route::post('sendSms',[UserDetailController::class, 'sendSms'])->name('user.sendSms');
    Route::post('guideline/delete/{id}', [GuidelineController::class, 'destroy']);
    Route::resource('guideline', GuidelineController::class);
    Route::resource('user', UserController::class);
    Route::resource('back-end', BackendUserController::class);
    Route::resource('topic', TopicController::class);
    Route::resource('type', TypeController::class);


    Route::prefix('code')->as('code.')->group(function () {
        Route::post('generate', [PregnantCodeController::class, 'generate'])->name('generate');
        Route::get('exportAll', [PregnantCodeController::class, 'exportAll'])->name('exportAll');
        Route::get('exportUsedCode', [PregnantCodeController::class, 'exportUsedCode'])->name('exportUsedCode');
        Route::get('exportUnusedCode', [PregnantCodeController::class, 'exportUnusedCode'])->name('exportUnusedCode');
    });

    Route::resource('pregnant-code', PregnantCodeController::class);



});

