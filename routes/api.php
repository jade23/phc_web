<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UsersController;
use App\Http\Controllers\API\GuidelinesController;
use App\Http\Controllers\API\GeneralController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1'], function () {
    Route::post('/login', [UsersController::class, 'login']);
    Route::post('/register', [UsersController::class, 'register']);
    Route::post('/checkUserEmail', [UsersController::class, 'checkUserEmail']);
    Route::get('download/terms-and-conditions', [GeneralController::class, 'downloadTermsAndCondition']);

    Route::middleware(['auth:api'])->group(function () {
        Route::get('/logout', [UsersController::class, 'logout']);

        Route::get('guidelines', [GuidelinesController::class, 'index']);
        Route::get('guideline/{id}', [GuidelinesController::class, 'show']);

        Route::post('updateUserDetails', [UsersController::class, 'updateUserDetails']);
        // Route::post('/upload-user-image', [UsersController::class, 'updateUserImage']);

        Route::post('receive-user-current-location', [UsersController::class, 'receiveUserCurrentLocation']);

        Route::post('firebase', [UsersController::class, 'received_fmc_token'])->name('received_fmc_token');
    });
});
