<?php $__env->startSection('current-page'); ?>
Code
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Codes</h1>
        <p class="card-category">View Code List</p>
        <div class="card-body">

            <div class="row">

                
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Code List</h5>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-default" data-toggle="modal"
                                data-target="#generateCodeModal">
                                Generate Code
                            </button>
                            <button class="btn btn-primary" data-toggle="modal"
                            data-target="#exportModal">Export Code</button>

                            <!-- Modal -->
                            <div class="modal fade" id="generateCodeModal" tabindex="-1" role="dialog"
                                aria-labelledby="generateCodeModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <form action="<?php echo e(route('code.generate')); ?>" method="POST">
                                        <?php echo csrf_field(); ?>
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="generateCodeModalLabel">Number to be generate</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <input type="number" name="number" id="" class="form-control"
                                                    onkeypress="if ( isNaN( String.fromCharCode(event.keyCode) )) return false;">
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Generate</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="exportModal" tabindex="-1" role="dialog"
                                aria-labelledby="exportModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exportModalLabel">Export Codes</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="d-flex justify-content-center">
                                                <a href="<?php echo e(route('code.exportUnusedCode')); ?>" class="btn btn-info mr-3">Unused Codes</a>
                                                <a href="<?php echo e(route('code.exportUsedCode')); ?>" class="btn btn-info mr-3">Used Codes</a>
                                                <a href="<?php echo e(route('code.exportAll')); ?>" class="btn btn-success mr-3">All Codes</a>
                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="margin: 2rem !important;">
                            <div class="table-responsive-lg">
                                <table class="table user_list">
                                    <thead class="text-primary">
                                        <th>Code</th>
                                        <th>Used By</th>
                                        <th>Used Date</th>
                                        <th>Created Date</th>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $codes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $code): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($code->code); ?></td>
                                                <td><?php echo e(getUserName($code->user_id)); ?></td>
                                                <td><?php echo e($code->used_date != null ?  date("F d, Y h:m A", strtotime($code->used_date)) : ''); ?></td>
                                                <td><?php echo e(date("F d, Y", strtotime($code->created_at))); ?></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                </table>
                                <?php echo $codes->links(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\work\sir_jr\phc_web\resources\views/pregnant_code/index.blade.php ENDPATH**/ ?>