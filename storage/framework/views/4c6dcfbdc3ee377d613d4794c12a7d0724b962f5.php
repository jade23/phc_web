<?php $__env->startSection('current-page'); ?>
Guidelines
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-css'); ?>
<style>
    .card-hover:hover {
        transform: scale(1.02);
    }

    .card-img-top {
        width: 100%;
        height: 15vw;
        object-fit: cover;
    }

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Health Care Guidelines</h1>
        <p class="card-category">Manage Heath Care Guidelines such as Announcement, Tips and Blog.</p>
        <div class="card-body">

            <div class="row">

                <div class="card card-plain">
                    <div class="card-header">
                        <h5 class="card-title">Create</h5>
                    </div>
                    <div class="card-body">
                        <div class="col-md-3">
                            <a href="<?php echo e(route('guideline.create')); ?>">
                                <div class="card card-hover text-center" style="width: 18rem;">
                                    <div class="card-body">
                                        <img class="card-img-top"
                                            src="<?php echo e(asset('assets/custom/images/logo/add_post.png')); ?>"
                                            alt="Card image cap">

                                        <h5 class="card-title">Add New Post</h5>
                                    </div>
                                </div>

                            </a>
                        </div>
                    </div>

                </div>

                <div class="col-xl-9">
                    <div class="card card-plain">
                        <div class="card-header">
                            <h5 class="card-title">Recently added Posts</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                <?php $__currentLoopData = $guidelines->take(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guideline): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                                <div class="col-md-3">
                                    <div class="card card-hover h-100">
                                        <img class="card-img-top" src="<?php echo e(asset($guideline->banner_path)); ?>"
                                            alt="post_banner">
                                        <div class="card-body">
                                            <h5 class="card-title"><?php echo e($guideline->title); ?></h5>
                                            <p class="card-text"><?php echo e($guideline->subtitle); ?></p>

                                        </div>
                                        <div class="card-footer">
                                            <a href="<?php echo e(route('guideline.show', $guideline->id)); ?>" class="btn btn-primary">View post</a>
                                        </div>
                                    </div>
                                </div>

                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-plain">
                <div class="card-header">
                    <h5 class="card-title">Posts</h5>
                    <div class="card-body">
                        <div class="row">

                            <?php $__currentLoopData = $guidelines->skip(2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $guideline): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>


                            <div class="col-md-3 mb-4">
                                <div class="card card-hover h-100">
                                    <img class="card-img-top" src="<?php echo e(asset($guideline->banner_path)); ?>"
                                        alt="post_banner">
                                    <div class="card-body">
                                        <h5 class="card-title"><?php echo e($guideline->title); ?></h5>
                                        <p class="card-text"><?php echo e($guideline->subtitle); ?></p>
                                    </div>
                                    <div class="card-footer">
                                        <a href="<?php echo e(route('guideline.show', $guideline->id)); ?>" class="btn btn-primary">View post</a>
                                    </div>
                                </div>
                            </div>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\work\sir_jr\phc_web\resources\views/guidelines/index.blade.php ENDPATH**/ ?>