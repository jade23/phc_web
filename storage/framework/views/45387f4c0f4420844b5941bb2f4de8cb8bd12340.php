<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    <!-- CSS Files -->
    <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/css/paper-dashboard.css?v=2.0.1')); ?>" rel="stylesheet" />
    <link href="<?php echo e(asset('assets/custom/app.css')); ?>" rel="stylesheet" />
    <?php echo $__env->yieldContent('page-css'); ?>
</head>

<body style="background-color: #f4f3ef">
    <div id="app">
        <div class="wrapper ">
            <?php if(auth()->guard()->check()): ?>
                <?php echo $__env->make('includes.side_menu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>



                <div class="main-panel">

                    <?php echo $__env->make('includes.navbar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                    <div class="content">
                        <div class="d-flex">
                            <div class="col-md-3">
                                <div class="justify-content-center">

                                    <?php echo $__env->make('includes.partials.toast-message', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                </div>
                            </div>
                        </div>
                        <?php echo $__env->yieldContent('content'); ?>
                    </div>

                    <footer id="footer" class="footer footer-black  footer-white ">
                        
                    </footer>
                </div>
            <?php else: ?>
                <?php echo $__env->yieldContent('content'); ?>
            <?php endif; ?>
        </div>
    </div>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--   Core JS Files   -->
    <script src="<?php echo e(asset('assets/js/core/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/core/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/core/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('assets/js/plugins/perfect-scrollbar.jquery.min.js')); ?>"></script>
    <!--  Google Maps Plugin    -->
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPDvR6Soz_eMehfqt89_0swbgfsWHvviY&callback=initMap&v=weekly&channel=2"
      async
    ></script>
    <!-- Chart JS -->
    <script src="<?php echo e(asset('assets/js/plugins/chartjs.min.js')); ?>"></script>
    <!--  Notifications Plugin    -->
    <script src="<?php echo e(asset('assets/js/plugins/bootstrap-notify.js')); ?>"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="<?php echo e(asset('assets/js/paper-dashboard.min.js?v=2.0.1')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/custom/app.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(asset('assets/custom/sweetalert.js')); ?>" type="text/javascript"></script>
    <script src="<?php echo e(mix('js/app.js')); ?>" type="text/javascript"></script>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('b98a38ccb1238bfc088e', {
          cluster: 'ap1'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            console.log(data);

            Swal.fire({
                icon: 'warning',
                title: 'Emergency Request!!',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                html:
                'Request From: <b>'+data.data.user_name+'</b><br>'+
                'User Location: <b>'+data.data.currect_location+'</b><br>,',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Done',
                allowOutsideClick: false,
            }).then((result) => {
                console.log(data.data.user_id);
                const url_string = "<?php echo e(route('user.sendSms')); ?>";
                $.ajax({
                    url: url_string,
                    type: 'post',
                    data: {
                        '_token': "<?php echo e(csrf_token()); ?>",
                        'user_id': data.data.user_id,
                        'location': data.data.currect_location,
                        'note' : result.value,
                    },
                    // beforeSend: function() {
                    //     $('body').addClass('loading');
                    // },
                    // complete: function(){
                    //     $('body').removeClass('loading');
                    // },
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                        icon: 'success',
                        title: data.success,
                        showConfirmButton: false,
                        timer: 1500
                        })
                        // $('#member_profile').html(data);
                    },


                });


                // loadCurrentPage();
            });


        });
      </script>



    <?php echo $__env->yieldContent('page-scripts'); ?>
</body>

</html>
<?php /**PATH C:\work\sir_jr\phc_web\resources\views/layouts/app.blade.php ENDPATH**/ ?>