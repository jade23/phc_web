<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo pb-3">
        <a href="<?php echo e(route('home')); ?>" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="../assets/img/logo-small.png">
            </div>
        </a>
        <div class="justify-content-between">
            <a href="<?php echo e(route('home')); ?>">
            <div class="pt-3">Pregnance Health Care</div>
        </a>
        </div>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="<?php echo e(in_array(Route::currentRouteName(), ['home']) ? 'active':''); ?>">
                <a href="<?php echo e(route('home')); ?>">
                    <i class="nc-icon nc-bank"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="<?php echo e(in_array(Route::currentRouteName(), ['guideline.index', 'guideline.show', 'guideline.edit', 'guideline.store', 'guideline.update', 'guideline.destroy']) ? 'active':''); ?>">
                <a href="<?php echo e(route('guideline.index')); ?>">
                    <i class="nc-icon nc-diamond"></i>
                    <p>Health Care guidelines</p>
                </a>
            </li>
            <li class="<?php echo e(in_array(Route::currentRouteName(), ['user.index', 'user.show', 'user.edit', 'user.store', 'user.update', 'user.destroy']) ? 'active':''); ?>">
                <a href="<?php echo e(route('user.index')); ?>">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Users</p>
                </a>
            </li>
            
            <?php if(auth()->user()->is_super_admin): ?>
            <li class="<?php echo e(in_array(Route::currentRouteName(), ['back-end.index', 'back-end.show', 'back-end.edit', 'back-end.store', 'back-end.update', 'back-end.destroy']) ? 'active':''); ?>">
                <a href="<?php echo e(route('back-end.index')); ?>">
                    <i class="nc-icon nc-hat-3"></i>
                    <p>Admin</p>
                </a>
            </li>
            <?php endif; ?>
            <li class="<?php echo e(in_array(Route::currentRouteName(), ['setting.index']) ? 'active':''); ?>">
                <a href="<?php echo e(route('setting.index')); ?>">
                    <i class="nc-icon nc-settings"></i>
                    <p>Settings - Statistics</p>
                </a>
            </li>
            <hr>
            <li>
                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="nc-icon nc-button-power"></i>
                    <p>Log Out</p>
                </a>
                <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" class="d-none">
                    <?php echo csrf_field(); ?>
                </form>
            </li>
        </ul>
    </div>
</div>
<?php /**PATH C:\work\sir_jr\phc_web\resources\views/includes/side_menu.blade.php ENDPATH**/ ?>