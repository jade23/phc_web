<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">TOPIC</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="card-header">
                                <h5 class="card-title">Topic List</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>ID</th>
                                            <th>TOPIC</th>
                                            <th># OF RELATED GUIDELINES</th>
                                            
                                        </thead>
                                        <tbody>
                                            <?php $__currentLoopData = $topics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $topic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <tr>
                                                    <td><?php echo e($topic->id); ?></td>
                                                    <td><?php echo e($topic->name); ?></td>
                                                    <td><?php echo e(count($topic->guidelines)); ?></td>
                                                    
                                                </tr>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php /**PATH C:\work\sir_jr\phc_web\resources\views/settings/guideline/topic.blade.php ENDPATH**/ ?>