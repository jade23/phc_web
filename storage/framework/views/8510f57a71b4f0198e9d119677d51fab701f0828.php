<?php $__env->startComponent('mail::message'); ?>
# Welcome to <?php echo e(config('app.name')); ?>


This mail is to notify you that we have received your application to be a backend user of our app.

Please wait atleast 1-2 working days as administrator review your application. We will send you another notification after we validate your account.


Account Details

Email: <?php echo e($request->email); ?>


Password: <?php echo e($request->password); ?>


Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH C:\work\sir_jr\phc_web\resources\views/emails/notifyUserAttemptRegister.blade.php ENDPATH**/ ?>