<?php $__env->startComponent('mail::message'); ?>
# Welcome to <?php echo e(config('app.name')); ?>


We have successfully created your account to use.
Please check details below:


Email : <?php echo e($user->email); ?>


Password : <?php echo e($password); ?>


<?php $__env->startComponent('mail::button', ['url' => config('app.url') ]); ?>
Login
<?php if (isset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e)): ?>
<?php $component = $__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e; ?>
<?php unset($__componentOriginalb8f5c8a6ad1b73985c32a4b97acff83989288b9e); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>

Thanks,<br>
<?php echo e(config('app.name')); ?>

<?php if (isset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d)): ?>
<?php $component = $__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d; ?>
<?php unset($__componentOriginal2dab26517731ed1416679a121374450d5cff5e0d); ?>
<?php endif; ?>
<?php echo $__env->renderComponent(); ?>
<?php /**PATH C:\work\sir_jr\phc_web\resources\views/emails/welcomeAdminUser.blade.php ENDPATH**/ ?>