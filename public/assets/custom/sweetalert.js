document.addEventListener('DOMContentLoaded', function () {
    //content goes here



 });

function removeTopic(id){
    const topic_destroy = "{{ route('topic.destroy)}}";
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax(
                {
                    url: "topic/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                    },
                    success: function (res){
                        successMsg(res)
                    },
                    error: function(err) {
                        errorMsg(err)
                    },
                    // complete: function() {
                    //     location.reload();
                    // },
                });
        }
    })
}

function removeType(id){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
        if (result.value) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax(
                {
                    url: "user/"+id,
                    type: 'DELETE',
                    data: {
                        "id": id,
                    },
                    success: function (res){
                        successMsg(res)
                    },
                    error: function(err) {
                        errorMsg(err)
                    },
                });
        }
    })
}

function errorMsg(params) {
    Swal.fire({
        icon: params.status,
        title: 'Oops...',
        text: params.message
      })
}

function successMsg(params) {
    Swal.fire({
        icon: params.status,
        title: 'Success!',
        text: params.message,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Done',
        allowOutsideClick: false,
      }).then((result) => {
        if (result.value) {
            loadCurrentPage();
        }
      });
}

function confirmEmergencyAlert(data){
    Swal.fire({
        icon: 'warning',
        title: 'Emergency Request!!',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        html:
        'Request From: <b>'+data.user_name+'</b><br>'+
        'User Location: <b>'+data.currect_location+'</b><br>,',
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Done',
        allowOutsideClick: false,
      }).then((result) => {
        if (result.value) {
            console.log(result.value);
            loadCurrentPage();
        }
      });
}

function loadCurrentPage() {
    location.reload();
}


