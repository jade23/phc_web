@component('mail::message')
# Account Status Update

We would like to inform your account at Pregnancy Health Care was updated by Administrator.

Current Status : {{ $status }}

@component('mail::button', ['url' => config('app.url') ])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
