@component('mail::message')
# Welcome to {{ config('app.name') }}

We have successfully created your account to use.
Please check details below:


Email : {{$user->email}}

Password : {{$password}}

@component('mail::button', ['url' => config('app.url') ])
Login
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
