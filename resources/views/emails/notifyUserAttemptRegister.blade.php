@component('mail::message')
# Welcome to {{ config('app.name') }}

This mail is to notify you that we have received your application to be a backend user of our app.

Please wait atleast 1-2 working days as administrator review your application. We will send you another notification after we validate your account.


Account Details

Email: {{$request->email}}

Password: {{$request->password}}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
