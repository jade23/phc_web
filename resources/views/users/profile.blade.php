@extends('layouts.app')

@section('current-page')
Users
@endsection

@section('content')
<div class="content">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-user">
                <div class="image">
                    <img src="../assets/img/damir-bosnjak.jpg" alt="...">
                </div>
                <div class="card-body">
                    <div class="author">
                        <a href="#">
                            <img class="avatar border-gray" src="../assets/img/mike.jpg" alt="...">
                            <h5 class="title">{{ $user->name }}</h5>
                        </a>
                        <p class="description">
                            {{ $user->email }}
                        </p>
                        <p>
                            @if ($user->user_details->status == true)
                                <span class="badge badge-primary">Active</span>
                            @else
                            <span class="badge badge-danger">Deactive</span>
                            @endif

                        </p>
                    </div>
                </div>
                <div class="card-footer">
                    <hr>
                    <div class="button-container">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-6 ml-auto">
                                <h6><b>{{ isset($user->user_details->birth_date) == true ? $user->user_details->birth_date : "n/a" }}</b><br><small>Birthday</small>
                                </h6>
                            </div>
                            <div class="col-lg-4 col-md-6 col-6 ml-auto mr-auto">
                                <h6><b>{{ isset($user->user_details->contact_number) == true ? $user->user_details->contact_number : "n/a" }}</b><br><small>Contact
                                        Number</small></h6>
                            </div>
                            <div class="col-lg-3 mr-auto">
                                <h6><b>{{ isset($user->user_details->check_up) == true ? $user->user_details->check_up : "n/a" }}</b><br><small>Up
                                        Coming Check Up</small></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Team Members</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled team-members">
                        <li>
                            <div class="row">
                                <div class="col-md-2 col-2">
                                    <div class="avatar">
                                        <img src="../assets/img/faces/ayo-ogunseinde-2.jpg" alt="Circle Image"
                                            class="img-circle img-no-padding img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-7 col-7">
                                    DJ Khaled
                                    <br />
                                    <span class="text-muted"><small>Offline</small></span>
                                </div>
                                <div class="col-md-3 col-3 text-right">
                                    <btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i
                                            class="fa fa-envelope"></i></btn>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2 col-2">
                                    <div class="avatar">
                                        <img src="../assets/img/faces/joe-gardner-2.jpg" alt="Circle Image"
                                            class="img-circle img-no-padding img-responsive">
                                    </div>
                                </div>
                                <div class="col-md-7 col-7">
                                    Creative Tim
                                    <br />
                                    <span class="text-success"><small>Available</small></span>
                                </div>
                                <div class="col-md-3 col-3 text-right">
                                    <btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i
                                            class="fa fa-envelope"></i></btn>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="row">
                                <div class="col-md-2 col-2">
                                    <div class="avatar">
                                        <img src="../assets/img/faces/clem-onojeghuo-2.jpg" alt="Circle Image"
                                            class="img-circle img-no-padding img-responsive">
                                    </div>
                                </div>
                                <div class="col-ms-7 col-7">
                                    Flume
                                    <br />
                                    <span class="text-danger"><small>Busy</small></span>
                                </div>
                                <div class="col-md-3 col-3 text-right">
                                    <btn class="btn btn-sm btn-outline-success btn-round btn-icon"><i
                                            class="fa fa-envelope"></i></btn>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div> --}}
        </div>
        <div class="col-md-8">
            <div class="card card-user">
                <div class="card-header">
                    <h5 class="card-title">Edit Profile</h5>
                </div>
                <div class="card-body">
                    {{-- <form action="{{ route('user.update', $user->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT') --}}
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input type="text" class="form-control  @error('name') is-invalid @enderror"
                                        id="name" name="name" placeholder="Name" value="{{ $user->name }}" readonly>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 pl-1">
                                <div class="form-group">
                                    <label for="email">Email address</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror"
                                        id="email" name="email" placeholder="Email" value="{{ $user->email }}" readonly>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="d-flex">
                                    <label for="is_pregnant">Is Pregnant ?</label> &nbsp; &nbsp;
                                    <input type="checkbox" name="is_pregnant" id="is_pregnant"
                                        {{ $user->user_details->is_pregnant == true ? 'checked' : '' }} disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control @error('address') is-invalid @enderror"
                                        id="address" name="address" placeholder="Home Address"
                                        value="{{ $user->user_details->address }}" readonly>
                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pr-1">
                                <div class="form-group">
                                    <label>Birth Date</label>
                                    <input type="date" class="form-control @error('birth_date') is-invalid @enderror"
                                        id="birth_date" name="birth_date" value="{{ $user->user_details->birth_date }}" readonly>
                                    @error('birth_date')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input type="text"
                                        class="form-control @error('contact_number') is-invalid @enderror"
                                        id="contact_number" name="contact_number" placeholder="Contact Number"
                                        value="{{ $user->user_details->contact_number }}" readonly>
                                    @error('contact_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            {{-- <div class="col-md-4 pl-1">
                                <div class="form-group">
                                    <label>Check Up Date</label>
                                    <input type="date" class="form-control @error('check_up') is-invalid @enderror"
                                        id="check_up" name="check_up" value="{{ $user->user_details->check_up }}" readonly>
                                    @error('check_up')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Husband Name</label>
                                    <input type="text" class="form-control @error('h_name') is-invalid @enderror"
                                        id="h_name" name="h_name" placeholder="Husband Name"
                                        value="{{ $user->user_details->husband_name }}" readonly>
                                    @error('h_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="h_contact_number">Husband Contact Number</label>
                                    <input type="text"
                                        class="form-control @error('h_contact_number') is-invalid @enderror"
                                        id="h_contact_number" name="h_contact_number"
                                        placeholder="Husband Contact Number"
                                        value="{{ $user->user_details->husband_contact_number }}" readonly>
                                    @error('h_contact_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-6">
                                <div class="form-group" id="repeaterFieldDiv">
                                    <div>
                                        <label for="med">Medical In-take</label>
                                    </div>
                                    <div class="mb-2">
                                        <button type="button" class="btn btn-rounded btn-info" id="addRow"><i
                                                class="fe fe-plus"></i> Add Row</button>
                                    </div>
                                    <div id="repeaterFields">
                                        @if (count($user->medical_intake) > 0)
                                        @foreach ($user->medical_intake as $item)
                                        <div class="input-group mb-2">

                                            <input type="text" class="form-control" name="med[]"
                                                value="{{ $item->name }}" />

                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-default removeRow"><i
                                                        class="nc-icon nc-simple-remove"></i></button>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="input-group mb-2">

                                            <input type="text" class="form-control" name="med[]"
                                                value="" />

                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-default removeRow"><i
                                                        class="nc-icon nc-simple-remove"></i></button>
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="update ml-auto mr-auto">
                                {{-- <button type="submit" class="btn btn-primary btn-round">Update Profile</button> --}}

                                <form action="{{route('user.updateUserStatus', $user->id)}}" method="POST">
                                    @csrf
                                    {{-- @method('PUT') --}}
                                    @if ($user->user_details->status == true)
                                    <input type="hidden" name="status" value="0">
                                    <button type="submit" class="btn btn-danger btn-round">Deactivate Account</button>
                                    @else
                                    <input type="hidden" name="status" value="1">
                                    <button type="submit" class="btn btn-primary btn-round">Activate Account</button>
                                    @endif



                                </form>
                            </div>
                        </div>
                    {{-- </form> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page-scripts')
    <script>

$(document).ready(function () {
        $('#addRow').on('click', function () {
            addRow();
        });

        $('#repeaterFields').on('click', '.removeRow', function () {
            $(this).parent().parent().remove();
        })

        function addRow() {
            var fieldRow = '<div class="input-group mb-2">' +
                '<input type="text" class="form-control" name="med[]"/>' +
                '<div class="input-group-append">' +
                '<button type="button" class="btn btn-default removeRow"><i class="nc-icon nc-simple-remove"></i></button>' +
                '</div>' +
                '</div>';
            $('#repeaterFields').append(fieldRow);
        };

    });

    function updateUserStatus() {
        console.log('as');
        document.getElementById("updateUserStatus").submit();
    }
    </script>
@endsection
