@extends('layouts.app')

@section('current-page')
Users
@endsection

@section('page-css')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="{{ asset('css/dropify.css')}}" rel="stylesheet">
@endsection

@section('content')
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Users</h1>
        <p class="card-category">View Users List</p>
        <div class="card-body">

            <div class="row">

                {{-- User List --}}
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">User List</h5>
                            <div>
                                <button class="btn btn-outline-primary btn-round" data-toggle="modal"
                                    data-target="#exampleModal">Add User</button>

                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1"
                                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Add User</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <form action=" {{ route('user.store') }}" enctype="multipart/form-data" method="POST">
                                                @csrf
                                                <div class="modal-body">
                                                    {{-- <div class="row"> --}}
                                                        {{-- <div class="col-md-4">
                                                            <label for="profile_picture">Profile Picture</label>
                                                            <input class="dropify" type="file" name="profile_picture"
                                                                id="profile_picture" data-height="300"
                                                                data-allowed-file-extensions="jpeg jpg png" />
                                                        </div>
                                                        <div class="col-md-8"> --}}
                                                            <div class="card-body">
                                                                <div class="row">
                                                                    <div class="col-md-6 pr-1">
                                                                        <div class="form-group">
                                                                            <label for="">Name</label>
                                                                            <input type="text"
                                                                                class="form-control  @error('name') is-invalid @enderror"
                                                                                id="name" name="name" placeholder="Name">
                                                                            @error('name')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 pl-1">
                                                                        <div class="form-group">
                                                                            <label for="email">Email address</label>
                                                                            <input type="email"
                                                                                class="form-control @error('email') is-invalid @enderror"
                                                                                id="email" name="email" placeholder="Email">
                                                                            @error('email')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 pr-1">
                                                                        <div class="form-group">
                                                                            <label for="">Password</label>
                                                                            <input type="password"
                                                                                class="form-control  @error('password') is-invalid @enderror"
                                                                                id="password" name="password" placeholder="Password">
                                                                            @error('password')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 pl-1">
                                                                        <div class="form-group">
                                                                            <label for="password_confirmation">Confirm Password</label>
                                                                            <input type="password"
                                                                                class="form-control @error('password_confirmation') is-invalid @enderror"
                                                                                id="password_confirmation" name="password_confirmation" placeholder="Confirm Password">
                                                                            @error('password_confirmation')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="d-flex">
                                                                            <label for="is_pregnant">Is Pregnant ?</label>
                                                                            &nbsp; &nbsp;
                                                                            <input type="checkbox" name="is_pregnant"
                                                                                id="is_pregnant">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <div class="form-group">
                                                                            <label>Address</label>
                                                                            <input type="text"
                                                                                class="form-control @error('address') is-invalid @enderror"
                                                                                id="address" name="address"
                                                                                placeholder="Home Address">
                                                                            @error('address')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6 pr-1">
                                                                        <div class="form-group">
                                                                            <label>Birth Date</label>
                                                                            <input type="date"
                                                                                class="form-control @error('birth_date') is-invalid @enderror"
                                                                                id="birth_date" name="birth_date">
                                                                            @error('birth_date')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror

                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label>Contact Number</label>
                                                                            <input type="text"
                                                                                class="form-control @error('contact_number') is-invalid @enderror"
                                                                                id="contact_number" name="contact_number"
                                                                                placeholder="Contact Number">
                                                                            @error('contact_number')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror

                                                                        </div>
                                                                    </div>
                                                                    {{-- <div class="col-md-4 pl-1">
                                                                        <div class="form-group">
                                                                            <label>Check Up Date</label>
                                                                            <input type="date"
                                                                                class="form-control @error('check_up') is-invalid @enderror"
                                                                                id="check_up" name="check_up">
                                                                            @error('check_up')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div> --}}
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="">Husband Name</label>
                                                                            <input type="text"
                                                                                class="form-control @error('h_name') is-invalid @enderror"
                                                                                id="h_name" name="h_name"
                                                                                placeholder="Husband Name">
                                                                            @error('h_name')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label for="h_contact_number">Husband Contact
                                                                                Number</label>
                                                                            <input type="text"
                                                                                class="form-control @error('h_contact_number') is-invalid @enderror"
                                                                                id="h_contact_number"
                                                                                name="h_contact_number"
                                                                                placeholder="Husband Contact Number">
                                                                            @error('h_contact_number')
                                                                            <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $message }}</strong>
                                                                            </span>
                                                                            @enderror
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        {{-- </div> --}}
                                                    {{-- </div> --}}
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body" style="margin: 2rem !important;">
                            <div class="table-responsive-lg">
                                <table class="table user_list">
                                    <thead class="text-primary">
                                        <th>NAME</th>
                                        <th>EMAIL</th>
                                        <th>PREGNANT ?</th>
                                        <th>CONTACT NUMBER</th>
                                        <th>DATE REGISTERED</th>
                                        <th>STATUS</th>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('page-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript">
    $(function () {

        $('.dropify').dropify({
            messages: {
                'default': 'Upload image',
            }
        });

        var table = $('.user_list').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('user.list') }}",
            columns: [{
                    data: 'name',
                    name: 'name',
                    render: function (datas) {
                        return '<a href="' + datas.route + '">' + datas.name + '</a>';
                    }
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'is_pregnant',
                    name: 'user_details.is_pregnant',
                    // searchable: false,
                },
                {
                    data: 'contact_number',
                    name: 'user_details.contact_number',
                    // searchable: false,
                },
                {
                    data: 'date_registered',
                    name: 'created_at',
                    sortOrder: 'desc',
                },
                {
                    data: 'status',
                    name: 'user_details.status',
                },

            ],
            'columnDefs': [{
                'targets': [0, 1, 2, 3, 5],
                /* column index */
                'orderable': false,
                /* true or false */
            }]
        });

    });

</script>
@endsection
