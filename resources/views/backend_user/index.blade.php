@extends('layouts.app')

@section('current-page')
Admin
@endsection

@section('content')
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Admin</h1>
        <p class="card-category">Manage Admins</p>
        <div class="card-body">

            <div class="row">
                {{-- Registration Form--}}
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Register an admin</h5>
                            <div class="card-body">
                                <form action="{{ route('back-end.store') }}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="is_backend_user" value="1">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                                name="name">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control @error('email') is-invalid @enderror"
                                                name="email">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="contact">Contact</label>
                                            <input type="text" class="form-control @error('contact') is-invalid @enderror"
                                                name="contact">
                                            @error('contact')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Password</label>
                                            <input type="text"
                                                class="form-control @error('password') is-invalid @enderror"
                                                name="password">
                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="text"
                                                class="form-control @error('password_confirmation') is-invalid @enderror"
                                                name="password_confirmation">
                                            @error('password_confirmation')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Register</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- End Registration Form--}}

                {{-- Back End User List --}}
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Admin List</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="text-primary">
                                        <th>#</th>
                                        <th>NAME</th>
                                        <th>EMAIL</th>
                                        <th>Contact #</th>
                                        <th>IS SUPER USER ?</th>
                                        <th>ACTION</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($users as $user)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->back_end_contact }}</td>
                                                <td>{{ $user->is_super_admin == true ? "YES" : "NO" }}</td>
                                                <td >
                                                    @if ($user->is_super_admin == true)
                                                        <button class="btn btn-secondary" disabled><i class="nc-icon nc-box"></i></button>
                                                    @else

                                                        @if ($user->status)
                                                            <a href="{{ route('user.updateBackendUserStatus', $user->id ) }}" class="btn btn-danger">Deactivate&nbsp;<i class="nc-icon nc-simple-remove"></i></a>
                                                        @else
                                                            <a href="{{ route('user.updateBackendUserStatus', $user->id ) }}" class="btn btn-success">Activate&nbsp;<i class="nc-icon nc-check-2"></i></a>
                                                        @endif

                                                        <button class="btn btn-danger" onclick="removeType({{ $user->id }})"><i class="nc-icon nc-box"></i></button>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
