@extends('layouts.app')

@section('current-page')
Guidelines
@endsection

@section('page-css')
<style>
    .card-hover:hover {
        transform: scale(1.02);
    }

    .card-img-top {
        width: 100%;
        height: 15vw;
        object-fit: cover;
    }

</style>
@endsection

@section('content')
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Health Care Guidelines</h1>
        <p class="card-category">Manage Heath Care Guidelines such as Announcement, Tips and Blog.</p>
        <div class="card-body">

            <div class="row">

                <div class="card card-plain">
                    <div class="card-header">
                        <h5 class="card-title">Create</h5>
                    </div>
                    <div class="card-body">
                        <div class="col-md-3">
                            <a href="{{ route('guideline.create') }}">
                                <div class="card card-hover text-center" style="width: 18rem;">
                                    <div class="card-body">
                                        <img class="card-img-top"
                                            src="{{ asset('assets/custom/images/logo/add_post.png') }}"
                                            alt="Card image cap">

                                        <h5 class="card-title">Add New Post</h5>
                                    </div>
                                </div>

                            </a>
                        </div>
                    </div>

                </div>

                <div class="col-xl-9">
                    <div class="card card-plain">
                        <div class="card-header">
                            <h5 class="card-title">Recently added Posts</h5>
                        </div>
                        <div class="card-body">

                            <div class="row">

                                @foreach ($guidelines->take(4) as $guideline)


                                <div class="col-md-3">
                                    <div class="card card-hover h-100">
                                        <img class="card-img-top" src="{{ asset($guideline->banner_path) }}"
                                            alt="post_banner">
                                        <div class="card-body">
                                            <h5 class="card-title">{{ $guideline->title }}</h5>
                                            <p class="card-text">{{ $guideline->subtitle }}</p>

                                        </div>
                                        <div class="card-footer">
                                            <a href="{{ route('guideline.show', $guideline->id) }}" class="btn btn-primary">View post</a>
                                        </div>
                                    </div>
                                </div>

                                @endforeach

                            </div>


                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-plain">
                <div class="card-header">
                    <h5 class="card-title">Posts</h5>
                    <div class="card-body">
                        <div class="row">

                            @foreach ($guidelines->skip(2) as $guideline)


                            <div class="col-md-3 mb-4">
                                <div class="card card-hover h-100">
                                    <img class="card-img-top" src="{{ asset($guideline->banner_path) }}"
                                        alt="post_banner">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $guideline->title }}</h5>
                                        <p class="card-text">{{ $guideline->subtitle }}</p>
                                    </div>
                                    <div class="card-footer">
                                        <a href="{{ route('guideline.show', $guideline->id) }}" class="btn btn-primary">View post</a>
                                    </div>
                                </div>
                            </div>

                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
