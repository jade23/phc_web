@extends('layouts.app')

@section('current-page')
Guidelines
@endsection

@section('page-css')
<link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="{{ asset('assets/summernote/dist/summernote.css') }}">
<link href="{{ asset('assets/dropify/dist/css/dropify.min.css') }}" rel="stylesheet" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<style>
    .custom-file-upload {
        border: 1px solid #ccc;
        display: inline-block;
        padding: 6px 12px;
        cursor: pointer;
    }

    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked+.slider {
        background-color: #2196F3;
    }

    input:focus+.slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked+.slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }

</style>
@endsection


@section('content')
<div class="card card-plain">
    <div class="card-header">
        <h1 class="card-title">Update Guideline</h1>
        {{-- <p class="card-category">New</p> --}}
        <div class="card-body">


            <div class="d-flex justify-content-center">
                <div class="col-md-8">

                    <div class="d-flex justify-content-center">

                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <h3>Update Post</h3>
                                </div>
                            </div>
                            <hr>
                            <form action="{{ route('guideline.update', $guideline->id) }}" enctype="multipart/form-data"
                                method="POST">
                                @csrf
                                @method('PUT')
                                <div class="col-auto">
                                    {{-- <button class="btn btn-primary">Submit and close</button> --}}
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('guideline.index') }}" class="btn btn-secondary">Close</a>
                                    <button type="button" onclick="showRemoveDialog({{$guideline->id}})"
                                        class="btn btn-danger">Remove</button>
                                </div>
                                <hr>
                                <div class="card-body">
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="form-group d-flex">
                                                <label for="target_users" class="col-md-4 col-form-label mt-2">Target
                                                    Users</label>
                                                <select name="target_users" id="target_users" class="form-control"
                                                    style="height: 50px">

                                                    <option value="general"
                                                        {{ $guideline->target_users == "general" ? "selected" : ""}}>
                                                        General</option>
                                                    <option value="1"
                                                        {{ $guideline->target_users == "1" ? "selected" : ""}}>Pregnant
                                                        Users</option>
                                                    <option value="0"
                                                        {{ $guideline->target_users == "0" ? "selected" : ""}}>Non
                                                        Pregnant Users</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <div class="form-group d-flex">
                                                <label class="col-md-3 col-form-label" for="post_type">Post Type</label>
                                                <select name="post_type" class="form-control" style="height: 50px"
                                                    id="post_type" onchange="showHideAttachment()">
                                                    @isset($types)
                                                    @foreach ($types as $type)
                                                    @if ($guideline->type->id == $type->id)
                                                    <option value="{{ $type->id }}" selected>{{ $type->name }}</option>
                                                    @else
                                                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                                                    @endif

                                                    @endforeach
                                                    @endisset

                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-5">
                                            Current File: <a href="#">{{ $guideline->attachment_name }}</a>
                                            <div id="attachmentDiv" style="display: none;">
                                                <input type="file" style="height: 50px"
                                                    class="form-control form-control-lg @error('attachment') is-invalid @enderror"
                                                    id="attachment" name="attachment" />
                                            </div>
                                            @if($errors->has('attachment'))
                                            <div class="error col-auto text-danger">{{ $errors->first('attachment') }}
                                            </div>
                                            @endif
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal"
                                                data-target="#addAttachment">
                                                Add Attachment/s
                                            </button>

                                            <!-- Modal -->
                                            <div class="modal fade" id="addAttachment" tabindex="-1" role="dialog"
                                                aria-labelledby="addAttachmentLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="addAttachmentLabel">Add
                                                                attachemnt/s
                                                            </h5>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-row">
                                                                <div class="form-group col-md-8" id="repeaterFieldDiv">
                                                                    <div>
                                                                        <label for="attachement">Attachment/s</label>
                                                                    </div>
                                                                    <div class="mb-2">
                                                                        <button type="button"
                                                                            class="btn btn-rounded btn-info"
                                                                            id="addRow"><i class="fe fe-plus"></i> Add
                                                                            Row</button>
                                                                    </div>
                                                                    <div id="repeaterFields">
                                                                        <div class="d-flex">
                                                                            <div class="input-group pb-2">
                                                                                <input type="file" style="height: 50px"
                                                                                    class="form-control form-control-lg"
                                                                                    id="attachment"
                                                                                    name="attachment[]" />
                                                                                <div class="input-group-append">
                                                                                    <button type="button"
                                                                                        class="btn btn-default btn-sm removeRow"><i
                                                                                            class="nc-icon nc-simple-remove"></i></button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary"
                                                                data-dismiss="modal">Done</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}

                                    </div>
                                    <div class="form-row col-md-12 pb-4">
                                        <div class="row">
                                            <label class="col-md-auto">Upload Banner</label>
                                            @if($errors->has('banner'))
                                            <div class="error col-auto text-danger">{{ $errors->first('banner') }}</div>
                                            @endif
                                        </div>
                                        <input type="file" name="banner" class="dropify" data-height="300"
                                            data-default-file="{{ asset($guideline->banner_path) }}" />
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="title">Title</label>
                                            <div class="col-md-8">
                                                <input type="text"
                                                    class="form-control @error('title') is-invalid @enderror"
                                                    placeholder="Title" id="title" name="title"
                                                    value="{{ $guideline->title }}">
                                                @error('title')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-form-label" for="subtitle">Subtitle</label>
                                            <div class="col-md-8">
                                                <textarea name="subtitle"
                                                    class="form-control @error('subtitle') is-invalid @enderror"
                                                    id="subtitle">{{ $guideline->subtitle }}</textarea>
                                                @error('subtitle')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group d-flex">
                                            <label class="col-form-label mr-5">Topic/s</label>
                                            <select class="form-control select2" name="topics[]" multiple>
                                                @isset($topics)
                                                @php $i = 0; @endphp
                                                @foreach ($topics as $topic)
                                                <option value="{{ $topic->id }}" @foreach ($guideline->topics as
                                                    $g_topics)
                                                    {{$g_topics->id == $topic->id ? 'selected': ''}}
                                                    @endforeach
                                                    >{{ $topic->name }}</option>
                                                @endforeach
                                                @endisset
                                            </select>
                                        </div>
                                        <div class="form-group row" id="contentDiv" style="display: block;">
                                            <div class="d-flex">
                                                <label class="col-form-label ml-4" for="content">Content</label>
                                                <div class="col-auto">
                                                    @if($errors->has('content'))
                                                    <div class="error col-auto text-danger">
                                                        {{ $errors->first('content') }}</div>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <textarea id="summernote" class="summernote"
                                                    name="content">{!! $guideline->content !!}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>

                </div>
            </div>


        </div>

    </div>
</div>
@endsection

@section('page-scripts')
<script src="{{ asset('assets/summernote/dist/summernote.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {

        showHideAttachment();

        $('#addRow').on('click', function () {
            addRow();
        });

        $('#repeaterFields').on('click', '.removeRow', function () {
            $(this).parent().parent().remove();
        })

        function addRow() {
            var fieldRow = '<div class="input-group mb-2">' +
                '<input type="file" class="form-control form-control-lg" id="attachment" name="attachment[]"/>' +
                '<div class="input-group-append">' +
                '<button type="button" class="btn btn-default btn-sm removeRow"><i class="nc-icon nc-simple-remove"></i></button>' +
                '</div>' +
                '</div>';
            $('#repeaterFields').append(fieldRow);
        };

    });

    $('.dropify').dropify();
    $('.select2').select2();
    $('.summernote').summernote({
        height: 300,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

    function showHideAttachment() {
        var post_type = document.getElementById("post_type");
        var attachmentDiv = document.getElementById("attachmentDiv");
        var contentDiv = document.getElementById("contentDiv");
        attachmentDiv.style.display = post_type.value == "2" ? "block" : "none";
        contentDiv.style.display = post_type.value == "2" ? "none" : "block";
        console.log(post_type.value);
    }

    function showRemoveDialog(guideline_id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {

                $.ajax({
                    url: '/guideline/delete/' + guideline_id,
                    type: 'post',
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'guideline_id': guideline_id,
                    },
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                            icon: 'success',
                            title: 'Successfully Removed!',
                            showConfirmButton: false,
                            timer: 1500
                        })

                        setTimeout(() => {
                            window.open("/guideline","_self")
                        }, 3000);

                    },


                });
            }
        })
    }


</script>
@endsection
