<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <!--     Fonts and icons     -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    <!-- CSS Files -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/paper-dashboard.css?v=2.0.1') }}" rel="stylesheet" />
    <link href="{{ asset('assets/custom/app.css') }}" rel="stylesheet" />
    @yield('page-css')
</head>

<body style="background-color: #f4f3ef">
    <div id="app">
        <div class="wrapper ">
            @auth
                @include('includes.side_menu')



                <div class="main-panel">

                    @include('includes.navbar')

                    <div class="content">
                        <div class="d-flex">
                            <div class="col-md-3">
                                <div class="justify-content-center">

                                    @include('includes.partials.toast-message')
                                </div>
                            </div>
                        </div>
                        @yield('content')
                    </div>

                    <footer id="footer" class="footer footer-black  footer-white ">
                        {{-- @include('includes.footer') --}}
                    </footer>
                </div>
            @else
                @yield('content')
            @endauth
        </div>
    </div>

    {{-- Scripts --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/core/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/perfect-scrollbar.jquery.min.js') }}"></script>
    <!--  Google Maps Plugin    -->
    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPDvR6Soz_eMehfqt89_0swbgfsWHvviY&callback=initMap&v=weekly&channel=2"
      async
    ></script>
    <!-- Chart JS -->
    <script src="{{ asset('assets/js/plugins/chartjs.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('assets/js/paper-dashboard.min.js?v=2.0.1') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/app.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/custom/sweetalert.js') }}" type="text/javascript"></script>
    <script src="{{ mix('js/app.js') }}" type="text/javascript"></script>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('814ac6bd4b400b79a166', {
          cluster: 'ap1'
        });

        var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function(data) {
            console.log(data);

            Swal.fire({
                icon: 'warning',
                title: 'Emergency Request!!',
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                html:
                'Request From: <b>'+data.data.user_name+'</b><br>'+
                'User Location: <b>'+data.data.currect_location+'</b><br>,',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Done',
                allowOutsideClick: false,
            }).then((result) => {
                console.log(data.data.user_id);
                const url_string = "{{ route('user.sendSms')}}";
                $.ajax({
                    url: url_string,
                    type: 'post',
                    data: {
                        '_token': "{{ csrf_token() }}",
                        'user_id': data.data.user_id,
                        'location': data.data.currect_location,
                        'note' : result.value,
                    },
                    // beforeSend: function() {
                    //     $('body').addClass('loading');
                    // },
                    // complete: function(){
                    //     $('body').removeClass('loading');
                    // },
                    success: function (data) {
                        console.log(data);
                        Swal.fire({
                        icon: 'success',
                        title: data.success,
                        showConfirmButton: false,
                        timer: 1500
                        })
                        // $('#member_profile').html(data);
                    },


                });


                // loadCurrentPage();
            });


        });
      </script>



    @yield('page-scripts')
</body>

</html>
