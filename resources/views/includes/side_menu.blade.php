<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo pb-3">
        <a href="{{ route('home') }}" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="../assets/img/logo-small.png">
            </div>
        </a>
        <div class="justify-content-between">
            <a href="{{ route('home') }}">
            <div class="pt-3">Pregnance Health Care</div>
        </a>
        </div>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ in_array(Route::currentRouteName(), ['home']) ? 'active':'' }}">
                <a href="{{ route('home') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="{{ in_array(Route::currentRouteName(), ['guideline.index', 'guideline.show', 'guideline.edit', 'guideline.store', 'guideline.update', 'guideline.destroy']) ? 'active':'' }}">
                <a href="{{ route('guideline.index') }}">
                    <i class="nc-icon nc-diamond"></i>
                    <p>Health Care guidelines</p>
                </a>
            </li>
            <li class="{{ in_array(Route::currentRouteName(), ['user.index', 'user.show', 'user.edit', 'user.store', 'user.update', 'user.destroy']) ? 'active':'' }}">
                <a href="{{ route('user.index') }}">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Users</p>
                </a>
            </li>
            {{-- <li class="{{ in_array(Route::currentRouteName(), ['pregnant-code.index']) ? 'active':'' }}">
                <a href="{{ route('pregnant-code.index') }}">
                    <i class="nc-icon nc-single-02"></i>
                    <p>Code</p>
                </a>
            </li> --}}
            @if (auth()->user()->is_super_admin)
            <li class="{{ in_array(Route::currentRouteName(), ['back-end.index', 'back-end.show', 'back-end.edit', 'back-end.store', 'back-end.update', 'back-end.destroy']) ? 'active':'' }}">
                <a href="{{ route('back-end.index') }}">
                    <i class="nc-icon nc-hat-3"></i>
                    <p>Admin</p>
                </a>
            </li>
            @endif
            <li class="{{ in_array(Route::currentRouteName(), ['setting.index']) ? 'active':'' }}">
                <a href="{{ route('setting.index') }}">
                    <i class="nc-icon nc-settings"></i>
                    <p>Settings - Statistics</p>
                </a>
            </li>
            <hr>
            <li>
                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="nc-icon nc-button-power"></i>
                    <p>Log Out</p>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</div>
