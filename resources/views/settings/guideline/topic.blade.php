<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">TOPIC</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- <div class="col-md-4">
                        <div class="card card-plain">
                            <form action="{{ route('topic.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-header">
                                    <h5 class="card-title">Create Topic</h5>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label for="topic_name">Topic Name</label>
                                            <input type="text" class="form-control @error('topic_name') is-invalid @enderror" name="topic_name">
                                            @error('topic_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="card-header">
                                <h5 class="card-title">Topic List</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>ID</th>
                                            <th>TOPIC</th>
                                            <th># OF RELATED GUIDELINES</th>
                                            {{-- <th>ACTION</th> --}}
                                        </thead>
                                        <tbody>
                                            @foreach ($topics as $topic)
                                                <tr>
                                                    <td>{{ $topic->id }}</td>
                                                    <td>{{ $topic->name }}</td>
                                                    <td>{{ count($topic->guidelines) }}</td>
                                                    {{-- <td>
                                                        <button class="btn btn-danger" onclick="removeTopic({{ $topic->id }})"
                                                    @foreach ($fix_topic_ids as $g_topic )
                                                        {{$g_topic == $topic->id ? 'disabled': ''}}
                                                    @endforeach><i class="nc-icon nc-simple-remove"></i></button>
                                                    </td> --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
