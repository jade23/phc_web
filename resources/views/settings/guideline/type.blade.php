<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">TYPE</h5>
            </div>
            <div class="card-body">
                <div class="row">
                    {{-- <div class="col-md-4">
                        <div class="card card-plain">
                            <form action="{{ route('type.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-header">
                                    <h5 class="card-title">Create Type</h5>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-10">
                                        <div class="form-group">
                                            <label for="type_name">Type Name</label>
                                            <input type="text" class="form-control @error('type_name') is-invalid @enderror" name="type_name">
                                            @error('type_name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary">Add</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> --}}
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="card-header">
                                <h5 class="card-title">Type List</h5>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead class="text-primary">
                                            <th>ID</th>
                                            <th>Type</th>
                                            <th># OF RELATED GUIDELINES</th>
                                            {{-- <th>ACTION</th> --}}
                                        </thead>
                                        <tbody>
                                            @foreach ($types as $type)
                                                <tr>
                                                    <td>{{ $type->id }}</td>
                                                    <td>{{ $type->name }}</td>
                                                    <td>{{ count($type->guidelines) }}</td>
                                                    {{-- <td>
                                                        <button class="btn btn-danger" onclick="removeType({{ $type->id }})"
                                                            @foreach ($fix_type_ids as $g_type )
                                                        {{$g_type == $type->id ? 'disabled': ''}}
                                                    @endforeach><i class="nc-icon nc-simple-remove"></i></button>
                                                    </td> --}}
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
