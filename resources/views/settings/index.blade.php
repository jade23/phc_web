@extends('layouts.app')

@section('current-page')
Settings
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-plain">
            <div class="card-header">
                <h1 class="card-title">Guidelines</h1>
                {{-- <p class="card-category">Settings for Making a Guidelines</p> --}}
            </div>
            @include('settings.guideline.topic')
            @include('settings.guideline.type')
        </div>
    </div>
</div>
@endsection
