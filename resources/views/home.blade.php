@extends('layouts.app')

@section('current-page')
Dashboard
@endsection

@section('page-css')
<style>
    #map {
        width: 100%;
    height: 600px;
}

/* Optional: Makes the sample page fill the window. */
html,
body {
  height: 100%;
  margin: 0;
  padding: 0;
}
</style>
@endsection

@section('content')
<div class="row">
    {{-- <code>my-event</code> --}}
    {{-- User --}}
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-primary">
                            <i class="nc-icon nc-circle-10 text-primary"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">App User</p>
                            <p class="card-title">{{ $userCount}}<p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Guidelines --}}
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-primary">
                            <i class="nc-icon nc-album-2 text-primary"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Guidelines</p>
                            <p class="card-title">{{$guidelineCount}}<p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Backend User --}}
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-body ">
                <div class="row">
                    <div class="col-5 col-md-4">
                        <div class="icon-big text-center icon-primary">
                            <i class="nc-icon nc-circle-10 text-primary"></i>
                        </div>
                    </div>
                    <div class="col-7 col-md-8">
                        <div class="numbers">
                            <p class="card-category">Admin</p>
                            <p class="card-title">{{$backendCount}}<p>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="card-footer ">
                <hr>
                <div class="stats">
                    <i class="fa fa-refresh"></i>
                    As of
                </div>
            </div> --}}
        </div>
    </div>




</div>

    <div id="map" style="display: block important!;"></div>
    <script>
        let map;

        function initMap(latitude,longitude) {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position){
                    var centerCoordinates = { lat: position.coords.latitude, lng: position.coords.longitude };
                    map = new google.maps.Map(document.getElementById("map"), {
                        center: centerCoordinates,
                        zoom: 8,
                    });

                    centerMarker(centerCoordinates, map);
                    markers(map);
                });
            } else {
                navigator.geolocation.getCurrentPosition(function(position){
                    map = new google.maps.Map(document.getElementById("map"), {
                        center: { lat: -34.397, lng: 150.644 },
                        zoom: 8,
                    });
                });
            }
        }
    </script>

@endsection
@section('page-scripts')
<script>

    function centerMarker(position, map) {
        var marker = new google.maps.Marker({
            position: position,
            map: map,
        })
    }

    function markers(map) {
        var datas = {!! $userLocation !!}
        showMarkers(datas, map)
    }

    function showMarkers(datas, map) {
        var infoWind = new google.maps.InfoWindow;

        Array.prototype.forEach.call(datas, function(data){
            console.log(data);


            if(data.latest_user_location){
                var content = document.createElement('div');
                var strong = document.createElement('strong');

                strong.textContent = data.name;
                content.appendChild(strong);

                // console.log(data);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data.latest_user_location.latitude, data.latest_user_location.longitude),
                    map: map,
                })

                var latlng = new google.maps.LatLng(data.latest_user_location.latitude, data.latest_user_location.longitude);
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng },  (results, status) =>{
                    if (status !== google.maps.GeocoderStatus.OK) {
                        alert(status);
                    }
                    // This is checking to see if the Geoeode Status is OK before proceeding
                    if (status == google.maps.GeocoderStatus.OK) {
                        // console.log('address: ' + results[0].formatted_address);
                        var address = results[0].formatted_address;
                        console.log('before return: ' + address);
                        // return address;

                        marker.addListener('click', function () {
                        infoWind.setContent(address);
                        infoWind.open(map, marker);
                    })
                    }
                });
            }
        })

    }

    function getReverseGeocodingData(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng },  (results, status) =>{
        if (status !== google.maps.GeocoderStatus.OK) {
            alert(status);
        }
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
            console.log(results);
            var address = (results[0].formatted_address);
        }
    });
}


</script>
@endsection
