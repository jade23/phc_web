<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserDetails extends Model
{
    use HasFactory;

    protected $casts = [
        'med_intake' => 'array',
      ];

    public function user_details()
    {
        return $this->belongsTo(User::class);
    }

    public function user_fmc()
    {
        return $this->hasOne(UserFMCToken::Class, 'user_id', 'user_id');
    }
}
