<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guideline extends Model
{
    use HasFactory;

    public function topics()
    {
        return $this->belongsToMany(Topic::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
