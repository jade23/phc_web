<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\Models\MedicalIntake;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'back_end_contact',
        'status',
        'is_backend_user',
        'is_super_admin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user_details()
    {
        return $this->hasOne(UserDetails::class);
    }

    public function medical_intake()
    {
        return $this->hasMany(MedicalIntake::class);
    }

    public function userLocation()
    {
        return $this->hasMany(UserLocation::class);
    }

    public function latestUserLocation()
    {
        return $this->hasOne(UserLocation::class)->latest();
    }

    public function code()
    {
        return $this->hasOne(PregnantCode::class);
    }
}
