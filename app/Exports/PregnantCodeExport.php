<?php

namespace App\Exports;

use App\Models\PregnantCode;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PregnantCodeExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function query()
    {
        return PregnantCode::query()->orderBy('created_at', 'DESC')->with('user');
    }

    public function map($code): array
    {
        if ($code->user != null) {
            $userName = $code->user->name;
        } else {
            $userName ='';
        }

        if ($code->used_date != null) {
            $usedDate =  date("F d, Y h:m A", strtotime($code->used_date));

        } else {
            $usedDate ='';
        }


        return [
            $code->code,
            $userName,
            $usedDate,
            date("F d, Y", strtotime($code->created_at)),

        ];
    }

    public function headings(): array
    {
        return [
            'Code',
            'User Name',
            'Used Date',
            'Date Generated',
        ];
    }
}
