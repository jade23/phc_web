<?php

namespace App\Exports;

use App\Models\PregnantCode;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PregnantCodeUnuseCodeExport implements FromQuery, WithMapping, WithHeadings
{
    use Exportable;

    public function query()
    {
        return PregnantCode::query()->orderBy('created_at', 'DESC')->whereNull('user_id');
    }

    public function map($code): array
    {

        return [
            $code->code,
            date("F d, Y", strtotime($code->created_at)),

        ];
    }

    public function headings(): array
    {
        return [
            'Code',
            'Date Generated',
        ];
    }
}
