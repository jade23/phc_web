<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Guideline;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::where('is_backend_user', false)->get();
        $guideline = Guideline::all();
        $backend = User::where('is_backend_user', true)->get();

        $userLocation = User::where('is_backend_user', 'false')->with('latestUserLocation')->get();

        // return $back;

        return view('home')->with(['userCount' => count($user), 'guidelineCount' => count($guideline), 'backendCount' => count($backend), 'userLocation' => $userLocation]);
    }
}
