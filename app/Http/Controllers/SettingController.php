<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Topic;

class SettingController extends Controller
{
    public function index()
    {
        $types = Type::with('guidelines')->get();
        $topics = Topic::with('guidelines')->get();

        $fix_topic_ids = [1,2,3];
        $fix_type_ids = [1,2];

        return view('settings.index', compact('types', 'topics', 'fix_topic_ids', 'fix_type_ids'));
    }
}
