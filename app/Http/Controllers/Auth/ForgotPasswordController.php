<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use DB;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

          /**

       * Write code on Method

       *

       * @return response()

       */

      public function submitResetPasswordForm(Request $request)

      {
          return $request;

          $request->validate([

              'email' => 'required|email|exists:users',

              'password' => 'required|string|min:6|confirmed',

              'password_confirmation' => 'required'

          ]);



          $updatePassword = DB::table('password_resets')

                              ->where([

                                'email' => $request->email,

                                'token' => $request->token

                              ])

                              ->first();



          if(!$updatePassword){

              return back()->withInput()->with('error', 'Invalid token!');

          }



          $user = User::where('email', $request->email)

                      ->update(['password' => Hash::make($request->password)]);



          DB::table('password_resets')->where(['email'=> $request->email])->delete();



          return redirect('/login')->with('message', 'Your password has been changed!');

      }
}
