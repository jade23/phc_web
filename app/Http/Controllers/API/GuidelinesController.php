<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Guideline;
use App\Models\Topic;

class GuidelinesController extends Controller
{
    public function index()
    {
        // $guidelines = Guideline::where('published', true)->with('topics', 'type')->orderBy('created_at', 'DESC')->get();
        $guidelines = array();

        $events = Topic::where('name', 'Events')->with(['guidelines' => function($query){
            $query->orderBy('created_at', 'DESC');
        }])->get();

        foreach ($events as $event) {
            $guidelines['events'] = $event->guidelines;
        }

        $tips = Topic::where('name', 'Tips')->with(['guidelines' => function($query){
            $query->orderBy('created_at', 'DESC');
        }])->get();

        foreach ($tips as $tip) {
            $guidelines['tips'] = $tip->guidelines;
        }

        $announcements = Topic::where('name', 'Announcements')->with(['guidelines' => function($query){
            $query->orderBy('created_at', 'DESC');
        }])->get();

        foreach ($announcements as $announcement) {
            $guidelines['announcements'] = $announcement->guidelines;
        }

        return response()->json(['guidelines' => $guidelines], 200);
    }

    public function show($id)
    {
        try {

            $guideline = Guideline::where('id', $id)->with('topics', 'type')->get();

            return response()->json([
                'success' => true,
                'guideline' => $guideline
            ]);

        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => 'error',
            ]);
        }
    }
}
