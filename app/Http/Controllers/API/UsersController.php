<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Models\User;
use Auth;
use App\Models\UserDetails;
use Log;
use App\Models\UserLocation;
use App\Events\RequestAmbulanceEvent;
use App\Models\UserFMCToken;
use App\Models\PregnantCode;
use Carbon\Carbon;

class UsersController extends Controller
{

    /**
     * Login api.
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = User::where('id', auth()->user()->id)->with('user_details')->first();
            // return $user;
            if ($user->user_details->status == true) {
                $success['token'] = $user->createToken('appToken')->accessToken;
                //After successfull authentication, notice how I return json parameters
                    return response()->json([
                    'success' => true,
                    'token' => $success,
                    'user' => $user
                ]);
            } else {
                return response()->json([
                    "success"=> false,
                    "message"=> "Account was deactivated."
                ]);
            }




        } else {
       //if authentication is unsuccessfull, notice how I return json parameters
          return response()->json([
            'success' => false,
            'message' => 'Invalid Email or Password',
        ], 401);
        }
    }

    public function register(Request $request)
    {
        $dateOfBirth = date('Y/m/d', strtotime($request->userBirthDate));

        $age = \Carbon\Carbon::parse($dateOfBirth)->age;

        if ($age >= 18) {
                # code...

                if($request->isPregnant == 'Yes'){
                    $code = PregnantCode::where('code', $request->isPregnantCode)->whereNull('user_id')->first();
                    if (!$code) {

                        return response()->json([
                            'NotFoundCode' => true,
                            'message' => 'Invalid Code. Please contact pregnancy@health_care.com for assistance.',
                        ], 401);
                    }
                }


            $validator = Validator::make($request->all(), [
                'email' => 'required|email|unique:users',
                'password' => 'required',
            ]);

            if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validator->errors(),
            ], 401);
            }

            $input = [
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ];

            $user = User::create($input);

            $user_details = new UserDetails;
            $user_details->user_id = $user->id;
            $user_details->birth_date = date('Y/m/d', strtotime($request->userBirthDate));
            $user_details->address = $request->userAddress;

            if($request->isPregnant == 'Yes'){
                $user_details->is_pregnant = true;

            } else {
                $user_details->is_pregnant = false;
            }

            $user_details->contact_number = $request->userContactNumber;
            $user_details->husband_name = $request->userHusbandName;
            $user_details->husband_contact_number = $request->userHusbandContactNumber;
            $user_details->check_up = date('Y/m/d', strtotime($request->userCheckUpDate));
            $user_details->save();

            if ($code) {
                $code->user_id = $user->id;
                $code->used_date = now();
                $code->save();
            }


            $userDetails = User::where('id', $user->id)->with('user_details')->first();

            $success['token'] = $user->createToken('appToken')->accessToken;

            return response()->json([
                'success' => true,
                'token' => $success,
                'user' => $userDetails,
            ]);

        } else {

            return response()->json([
                'restrict' => true,
                'message' => 'Underage Users are not allowed to use the app. Please contact pregnancy@health_care.com for assistance.',
            ], 401);

        }
    }

    public function logout(Request $res)
    {
      if (Auth::user()) {
        $userFMC = UserFMCToken::where('user_id', auth()->user()->id)->delete();

        $user = Auth::user()->token();
        $user->revoke();

        return response()->json([
          'success' => true,
          'message' => 'Logout successfully'
      ]);
      }else {
        return response()->json([
          'success' => false,
          'message' => 'Unable to Logout'
        ]);
      }
     }

     public function checkUserEmail(Request $request)
     {
         $user = User::where('email', $request->email)->with('user_details')->first();

         if ($user) {
             return response()->json(['status' => 'ok', 'user' => $user], 200);
         }

         return response()->json(['status' => 'failed'], 404);
     }

     public function updateUserDetails(Request $request)
     {
        // return $request;
        $user = User::find($request->userId);

        if ($user) {
          $user->name = $request->name;
            $user->save();

            $userDetails = UserDetails::where('user_id', $request->userId)->first();
            $userDetails->address = $request->address;

            $birth_date = date('Y/m/d', strtotime($request->birth_date));
            // $check_up = date('Y/m/d', strtotime($request->check_up));

            $userDetails->birth_date = $birth_date;
            $userDetails->contact_number = $request->contact_number;
            $userDetails->husband_name = $request->husband_name;
            $userDetails->husband_contact_number = $request->husband_contact_number;
            // $userDetails->check_up = $check_up;
            if($request->is_pregnant == 'Yes'){
                $userDetails->is_pregnant = true;
            } else {
                $userDetails->is_pregnant = false;
            }

            $userDetails->save();

            $userDetails = User::where('id', $request->userId)->with('user_details')->first();
            return response()->json([
                'success' => true,
                'user' => $userDetails
            ]);
        }

        // return response()->json(['status' => 'failed'], 464);
     }

     public function receiveUserCurrentLocation(Request $request)
     {
         try {

            $user = User::findOrFail($request->userId);

            if ($user) {


                $endpoint = "https://nominatim.openstreetmap.org/reverse?format=json&lat=".$request->latitude."&lon=".$request->longitude;
                $client = new \GuzzleHttp\Client();

                $response = $client->request('GET', $endpoint);

                // url will be: http://my.domain.com/test.php?key1=5&key2=ABC;

                $statusCode = $response->getStatusCode();
                $address = json_decode($response->getBody(), true);

                $userLocation = new UserLocation;
                $userLocation->user_id = $user->id;
                $userLocation->address = $address['display_name'];
                $userLocation->latitude = $request->latitude;
                $userLocation->longitude = $request->longitude;
                $userLocation->save();

                // \Log::info("Content at : " . $address);



                $data = [
                    'user_id' => $user->id,
                    'user_name' => $user->name,
                    'currect_location' => $userLocation->address,
                    'latitude' => $userLocation->latitude,
                    'longitude' => $userLocation->longitude,
                ];

                event(new RequestAmbulanceEvent($data));

                $userDetails = UserDetails::where('user_id', $user->id)->first();

            // SMS NOtification
            $sender = config('services.sms_sender');
            $sms_api_key = config('services.sms_api_key');
            $sms_api_secret = config('services.sms_api_secret');

            $now = Carbon::now()->format('F j, Y');



            $pre_define_sms_user = 'Emergency Alert - '. $now .' : User ( '. $data['user_name'].' ) with ID of ' . $data['user_id'] . ' sent an emergency request. Please check website application to confirm the request. You can also directly contact the user by this numbers. User Number: '. $userDetails->contact_number .'. Husband Number: '. $userDetails->husband_contact_number.'.';
            $user_sms = str_replace(" ", "%20", $pre_define_sms_user);

            $backEndUser = User::where([['status', true], ['is_backend_user', true]])->get();



            foreach ($backEndUser as $item) {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.promotexter.com/sms/send?apiKey=".$sms_api_key."&apiSecret=".$sms_api_secret."&from=".$sender."&to=".$item->back_end_contact."&text=".$user_sms);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);

            $response = curl_exec($ch);
            curl_close($ch);

                # code...
            }

            // End SMS NOtification



                return response()->json([
                    'success' => true,
                    'message' => 'Successfully sent coordinates. Please stand by where you are.',
                ]);

            }


        } catch (\Throwable $th) {
            Log::info("Error on: " .$th);

            return response()->json([
                'success' => false,
                'message' => 'Unable to find user data. Please clear app data or re-install the app.',
            ]);
        }
     }

    //  public function updateUserImage(Request $request)
    //  {

    //     try {
    //         $userDetails = UserDetails::where('user_id', $request->userId)->first();

    //         $image = new Image;
    //         $getImage = $request->file('image');
    //         $imageName = time().'.'.$getImage->extension();
    //         $image->move(public_path('images'), $imageName);


    //     } catch (\Throwable $th) {
    //         //throw $th;
    //         Log::info("Error on: " .$th);
    //         return response()->json([
    //             'success' => false,
    //             'message' => 'Unable to find user data. Please clear app data or re-install the app.',
    //         ]);
    //     }

    //  }


    public function received_fmc_token(Request $request)
    {
        $userFMC = UserFMCToken::where('user_id', $request->userId)->first();

        if($userFMC){
            $userFMC->_fmc_token = $request->_fmc_token;
            $userFMC->save();
        } else {
            $newFmc = new UserFMCToken;
            $newFmc->user_id = $request->userId;
            $newFmc->_fmc_token = $request->_fmc_token;
            $newFmc->save();
        }

        return response()->json(['success' => 'true'], 200);



    }
}
