<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Response;

class GeneralController extends Controller
{
    public function downloadTermsAndCondition()
    {
        // $url = Storage::url('content-in-terms-and-regulation.pdf');
        $pathToFile = public_path() . '/storage/content-in-terms-and-regulation.pdf';
        return response()->download($pathToFile);

        // return (new Response($pathToFile, 200));
    }
}
