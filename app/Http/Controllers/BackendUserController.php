<?php

namespace App\Http\Controllers;

use App\Models\BackendUser;
use Illuminate\Http\Request;
use App\Models\User;
use Hash;
use App\Mail\WelcomeAdminUser;
use Illuminate\Support\Facades\Mail;

class BackendUserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('is_backend_user', true)->get();
        return view('backend_user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateData($request);

        $is_backend_user = false;
        $is_super_admin = false;

        if ($request->is_backend_user) {
            $is_backend_user = true;
        }

        if ($request->is_super_admin) {
            $is_super_admin = true;
        }

        $password = $request->password;

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'is_backend_user' => $is_backend_user,
            'back_end_contact' => $request->contact,
            'is_super_admin' => $is_super_admin,
        ]);

        Mail::to($user->email)->send(new WelcomeAdminUser($user, $password));

        if($user){
            return redirect()->back()->with('success', 'Successfully registered!');
        }

        return redirect()->back()->with('error', 'Ops! Something went wrong.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BackendUser  $backendUser
     * @return \Illuminate\Http\Response
     */
    public function show(BackendUser $backendUser)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BackendUser  $backendUser
     * @return \Illuminate\Http\Response
     */
    public function edit(BackendUser $backendUser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BackendUser  $backendUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BackendUser $backendUser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BackendUser  $backendUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(BackendUser $backendUser)
    {
        //
    }

    public function validateData($data)
    {
        $this->validate($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'contact' => ['required', 'numeric'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }
}
