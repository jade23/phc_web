<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserDetails;
use App\Models\MedicalIntake;
use DataTables;

use App\Mail\BackendUserStatusChangeAlert;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('users.index');
    }

    public function list()
    {
        $users = User::where([
            ['is_backend_user', false],
            ['is_super_admin', false],
        ])->with('user_details');
        // return $users->get();
        return Datatables::of($users)
            ->editColumn('name', function ($data) {
                $datas = [
                    'route' => route('user.show', $data->id),
                    'name' => $data->name,
                ];
                return $datas;
            })->editColumn('email', function ($data) {
                return $data->email;
            })->editColumn('is_pregnant', function ($data) {
                // if (isset($data->user_details->is_pregnant)) {
                //     return $data->user_details->is_pregnant;
                // }
                // return 'n/a';
                if($data->user_details->is_pregnant == 1){
                    return 'Yes';
                }else{
                    return 'No';
                }
            })->editColumn('contact_number', function ($data) {
                if (isset($data->user_details->is_pregnant)) {
                    return $data->user_details->contact_number;
                }
                return 'n/a';
            })->editColumn('date_registered', function ($data) {
                return date('F d, Y', strtotime($data->created_at));
            })
            ->editColumn('status', function ($data) {
                // return $data->user_details->status;
                if($data->user_details->status == 1){
                    return 'Active';
                }else{
                    return 'Deactive';
                }
            })
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validateData($request);

        try {
            $user = new User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->save();

            $userDetails = new UserDetails;
            $userDetails->user_id = $user->id;
            $userDetails->birth_date = $request->birth_date;
            $userDetails->address = $request->address;
            $userDetails->contact_number = $request->contact_number;
            $userDetails->husband_name = $request->h_name;
            $userDetails->husband_contact_number = $request->h_contact_number;
            // $userDetails->check_up = $request->check_up;

            // if ($request->has('profile_picture')) {
            //     $userDetails->profile_picture = 'storage/'. $request->profile_picture->store('/user/profile_picture', 'public');
            // }


            if ($request->is_pregnant) {
                $userDetails->is_pregnant = true;
            } else {
                $userDetails->is_pregnant = false;
            }
            $userDetails->save();

            return redirect()->back()->with('success', 'Successfully added user!');

        } catch (\Throwable $th) {
            \Log::info("Error while creating user. Error on: " . $th);
            return redirect()->back()->with('error', 'Ops! Something went wrong. Please check data and try again.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->with('user_details', 'medical_intake')->first();
        // return $user;
        return view('users.profile', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;

        $this->validateData($request);

        $user = User::findOrFail($id);

        $user->name = $request->name;
        // check if email is already exisit except user current email
        if($user->email != $request->email){
            $user->email = $request->email;
        }

        $user_details = UserDetails::where('user_id', $user->id)->first();
        $user_details->address = $request->address;
        $user_details->birth_date = $request->birth_date;
        $user_details->contact_number = $request->contact_number;
        $user_details->check_up = $request->check_up;
        $user_details->husband_name = $request->h_name;
        $user_details->husband_contact_number = $request->h_contact_number;

        if ($request->is_pregnant) {
            $user_details->is_pregnant = true;
        } else {
            $user_details->is_pregnant = false;
        }

        $user->save();
        $user_details->save();

        if($request->med){
            MedicalIntake::where('user_id', $user->id)->delete();

            foreach ($request->med as $item) {
                $med_intake = new MedicalIntake;
                $med_intake->user_id = $user->id;
                $med_intake->name = $item;
                $med_intake->save();
            }
        } else {
            MedicalIntake::where('user_id', $user->id)->delete();
        }


        return redirect()->back()->with('success', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
    }

    public function validateData($data)
    {
        $this->validate($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['unique:users,email', 'required', 'string', 'email', 'max:255'],
            'password' => 'required|confirmed|min:6',
            'address' => ['required', 'string'],
            'birth_date' => ['required'],
            'contact_number' => ['required', 'numeric'],
            'h_name' => ['required'],
            'h_contact_number' => ['required'],
        ]);
    }

    public function updateBackendUserStatus($id)
    {
        $user = User::find($id);

        try {
            //code...

            if ($user->status) {
                $user->status = false;
                $user->save();

                $status = 'Deactivated';

                Mail::to($user->email)->send(new BackendUserStatusChangeAlert($status));

                return redirect()->back()->with('success', 'Successfully Deactivated');
            } else {
                $user->status = true;
                $user->save();

                $status = 'Activated';

                Mail::to($user->email)->send(new BackendUserStatusChangeAlert($status));

                return redirect()->back()->with('success', 'Successfully Activated');
            }

        } catch (\Throwable $th) {

            \Log::info("Error while update backend user status. Error: " . $th);
            return redirect()->back()->with('error', 'Unable to proccess due to encountered error.');
                //throw $th;
        }
    }

}
