<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        $validate = $this->validate($request, [
            'topic_name' => ['required', 'string', 'max:20', 'unique:topics,name'],
        ]);

        try {

            $type = new Topic;
            $type->name = $request->topic_name;
            $type->save();

            return redirect()->back()->with('success', 'Successfully added!');

        } catch (\Throwable $th) {
            Log::info("Error while saving topic for guideline. Error Message : " . $th);
            return redirect()->back()->with('error', 'Opss! Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit(Topic $topic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $topic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $topic = Topic::findOrFail($id)->delete();

        if ($topic) {
            return response()->json(['status' => 'success', 'message' => 'Topic was successfully deleted.'], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Topic was not able to remove.'], 200);
    }
}
