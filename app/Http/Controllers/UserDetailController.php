<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserDetails;
use Log;

class UserDetailController extends Controller
{
    public function updateUserStatus(Request $request, $id)
    {
        try {
            $user = UserDetails::where('user_id', $id)->first();

            $user->status = $request->status;
            $user->save();

            if ($request->status == 1) {
                return redirect()->back()->with('success', 'User was successfully activated!');
            } else {
                return redirect()->back()->with('success', 'User was successfully deactivated!');
            }

        } catch (\Throwable $th) {
            Log::info("Error on : " . $th);

            return redirect()->back()->with('error', 'Unable to proceed request.');
        }
    }


    public function sendSms(Request $request)
    {
        try {
            $user = UserDetails::where('user_id', $request->user_id)->first();

            $sender = config('services.sms_sender');
            $sms_api_key = config('services.sms_api_key');
            $sms_api_secret = config('services.sms_api_secret');

            if ($request->note == null) {
                $note = 'Please standby.';
            } else {
                $note = $request->note;
            }

            $pre_define_sms_user = 'Your ambulance has been dispatched and going to your location. One of our team will contact you for further assistance. Assistant Note: ' . $note;
            $user_sms = str_replace(" ", "%20", $pre_define_sms_user);
            //for user
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.promotexter.com/sms/send?apiKey=".$sms_api_key."&apiSecret=".$sms_api_secret."&from=".$sender."&to=".$user->contact_number."&text=".$user_sms);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);

            $response = curl_exec($ch);
            curl_close($ch);

            $pre_define_sms_husband = 'Your wife pressed the emergency request button. We are on our way to her current location. Her current location : ' . $request->location;
            $husband_sms = str_replace(" ", "%20", $pre_define_sms_husband);
            //for husband
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.promotexter.com/sms/send?apiKey=".$sms_api_key."&apiSecret=".$sms_api_secret."&from=".$sender."&to=".$user->husband_contact_number."&text=".$husband_sms);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);

            $response = curl_exec($ch);
            curl_close($ch);

            return response()->json(['success' => 'Successfully send a sms alert to the user.'], 200);

        } catch (\Throwable $th) {
            \Log::info("Error on : " + $th);

            // return response()->json(['success' => 'Successfully send a sms alert to the user.'], 200);
        }
    }
}
