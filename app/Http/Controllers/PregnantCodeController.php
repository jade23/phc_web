<?php

namespace App\Http\Controllers;

use App\Models\PregnantCode;
use Illuminate\Http\Request;
use App\Exports\PregnantCodeExport;
use App\Exports\PregnantCodeUsedCodeExport;
use App\Exports\PregnantCodeUnuseCodeExport;

class PregnantCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes = PregnantCode::with('user')->paginate(10);

        return view('pregnant_code.index', compact('codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PregnantCode  $pregnantCode
     * @return \Illuminate\Http\Response
     */
    public function show(PregnantCode $pregnantCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PregnantCode  $pregnantCode
     * @return \Illuminate\Http\Response
     */
    public function edit(PregnantCode $pregnantCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PregnantCode  $pregnantCode
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PregnantCode $pregnantCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PregnantCode  $pregnantCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(PregnantCode $pregnantCode)
    {
        //
    }

    public function generate(Request $request)
    {

        $unUseCode = PregnantCode::whereNull('user_id')->get();

        // return count($unUseCode)

        if (count($unUseCode) >= 0 && count($unUseCode) <= 10) {
            for ($i=0; $i < $request->number ; $i++) {
                # code...

                $generated_code = \Str::random(6);

                $check = PregnantCode::where('code', $generated_code)->first();
                while ($check) {
                    $generated_code = \Str::random(6);
                    $check = PregnantCode::where('code', $generated_code)->first();
                }
                $saveGeneratedCode = new PregnantCode;
                $saveGeneratedCode->code = $generated_code;
                $saveGeneratedCode->save();

            }

            return redirect()->route('pregnant-code.index');
        }
        return redirect()->back()->with('info', 'Please use the unused code first before generating new codes.');
    }

    public function exportAll()
    {
        $dateTimeExported = date('YmdHis');
        return (new PregnantCodeExport)->download('AllPregnantCode_'.$dateTimeExported.'.xlsx');
    }

    public function exportUsedCode()
    {
        $dateTimeExported = date('YmdHis');
        return (new PregnantCodeUsedCodeExport)->download('UsedPregnantCode_'.$dateTimeExported.'.xlsx');
    }

    public function exportUnusedCode()
    {
        $dateTimeExported = date('YmdHis');
        return (new PregnantCodeUnuseCodeExport)->download('UnusedPregnantCode_'.$dateTimeExported.'.xlsx');
    }
}
