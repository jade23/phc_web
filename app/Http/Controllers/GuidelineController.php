<?php

namespace App\Http\Controllers;

use App\Models\Guideline;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Topic;
use App\Models\Attachment;
use Log;
use App\Models\UserFMCToken;
use App\Models\UserDetails;


class GuidelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guidelines = Guideline::orderBy('updated_at', 'DESC')->get();
        return view('guidelines.index', compact('guidelines'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        $topics = Topic::all();
        return view('guidelines.create', compact('types', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validateData($request);
        try {
            $guideline = $this->saveGuideline($request);


            // Push Notification on mobile app
            $registration_ids = [];
            $forSms = [];

            if ($guideline->target_users == 'general') {
                $user_fmc_token = UserFMCToken::all();

                foreach ($user_fmc_token as $item) {
                    $registration_ids[] = $item->_fmc_token;
                }

                $userSMS = UserDetails::where('status', true)->get();

                foreach ($userSMS as $item) {
                    $forSms[] = $item->contact_number;
                }
            }elseif ($guideline->target_users == '1') {
                $users = UserDetails::where([['is_pregnant', true], ['status', true]])->with('user_fmc')->get();
                // return $users;
                foreach ($users as $item) {
                    if ($item->user_fmc->_fmc_token) {
                        $registration_ids[] = $item->user_fmc->_fmc_token;
                    }
                    $forSms[] = $item->contact_number;

                }
            } else {
                $users = UserDetails::where([['is_pregnant', false], ['status', true]])->with('user_fmc')->get();

                foreach ($users as $item) {
                    $registration_ids[] = $item->user_fmc->_fmc_token;
                    $forSms[] = $item->contact_number;
                }
            }


            $authorization_key = "AAAAB220scU:APA91bEUS8wSzeUh905t8NBCge0CuyQKS41ND09sTD6Xwcmwa5PlRFSQaTWJYWWLNFbb5Ab8yVw0DLvgYdXFrfpQn9tXQATmma4kH07iVc6rOybCLqhxXLE78X_cFuz9df2GToGLp5S8";

            $finalPostArray = array('registration_ids' => $registration_ids,
                                    'notification' => array('body' => 'There\'s a new guideline posted with a title "'. $guideline->title .'". To know more about it, please check it on your PHC app dashboard for more details.',
                                                            'title' => 'New Posted Guidelines',
                                                            "image"=> asset($guideline->banner_path) ),
                                    "data"=> array("click_action"=> "FLUTTER_NOTIFICATION_CLICK",
                                                    "sound"=> "default",
                                                    "status"=> "done"));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://fcm.googleapis.com/fcm/send");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($finalPostArray));  //Post Fields
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: key='.$authorization_key));
            $server_output = curl_exec ($ch);
            curl_close ($ch);
            // return $server_output;

            // End Push Notification on mobile app

            // SMS NOtification
            $sender = config('services.sms_sender');
            $sms_api_key = config('services.sms_api_key');
            $sms_api_secret = config('services.sms_api_secret');


            $pre_define_sms_user = 'There\'s a new guideline posted with a title "'. $guideline->title .'". To know more about it, please check it on your PHC app dashboard for more details.';
            $user_sms = str_replace(" ", "%20", $pre_define_sms_user);

            foreach ($forSms as $item) {

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "https://api.promotexter.com/sms/send?apiKey=".$sms_api_key."&apiSecret=".$sms_api_secret."&from=".$sender."&to=".$item."&text=".$user_sms);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, FALSE);

            curl_setopt($ch, CURLOPT_POST, TRUE);

            $response = curl_exec($ch);
            curl_close($ch);

                # code...
            }

            // End SMS NOtification





            return redirect()->route('guideline.show', $guideline->id)->with('success', 'Post was successfully created!');
        } catch (\Throwable $th) {
            Log::info("Error while saving Guideline. Error at : " .$th);
            return redirect()->route('guideline.index')->with('error', 'Ops! Something went wrong.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guideline  $guideline
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $guideline = Guideline::where('id', $id)->with('type', 'topics')->first();
        // return $guideline;
        $types = Type::all();
        $topics = Topic::all();
        return view('guidelines.show', compact('guideline', 'types', 'topics'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Guideline  $guideline
     * @return \Illuminate\Http\Response
     */
    public function edit(Guideline $guideline)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Guideline  $guideline
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request;

        if ($request->post_type == 1) {
            $validate = $this->validate($request, [
                'title' => ['required', 'string'],
                'subtitle' => ['required', 'string'],
                'content' => ['required'],
            ]);
        }else {
            $validate = $this->validate($request, [
                'title' => ['required', 'string'],
                'subtitle' => ['required', 'string'],
            ]);
        }

        try {


            $guideline = $this->updateGuideline($request, $id);

            return redirect()->route('guideline.show', $guideline->id)->with('success', 'Post was successfully updated!');
        } catch (\Throwable $th) {
            Log::info("Error while saving Guideline. Error at : " .$th);
            return redirect()->route('guideline.index')->with('error', 'Ops! Something went wrong.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Guideline  $guideline
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Guideline::find($id)->delete($id);

        return redirect()->route('guideline.index')->with('success', 'Guideline post was successfully removed!');
    }

    public function validateData($data)
    {
        if ($data->post_type == 1) {
            $validate = $this->validate($data, [
                'title' => ['required', 'string'],
                'subtitle' => ['required', 'string'],
                'content' => ['required'],
                'banner' => ['required'],
            ]);
        }else {
            $validate = $this->validate($data, [
                'title' => ['required', 'string'],
                'subtitle' => ['required', 'string'],
                'banner' => ['required'],
                'attachment' => ['required'],
            ]);
        }
    }



    public function saveGuideline($data)
    {
        $guideline = new Guideline;


        $guideline->target_users = $data->target_users;
        $guideline->title = $data->title;
        $guideline->subtitle = $data->subtitle;
        $guideline->content = $data->content;

        // for banner
        $this->saveBanner($data, $guideline);

        // for post type
        $this->attachType($data, $guideline);

        // for attachment/s
        $this->saveAttachments($data, $guideline);
        $guideline->save(); //save data



        // for post topics
        $this->attachTopics($data, $guideline);



        return $guideline;
    }

    public function updateGuideline($data, $id)
    {
        $guideline = Guideline::findOrFail($id);


        $guideline->target_users = $data->target_users;
        $guideline->title = $data->title;
        $guideline->subtitle = $data->subtitle;
        $guideline->content = $data->content;

        // for banner
        $this->saveBanner($data, $guideline);

        // for post type
        $this->attachType($data, $guideline);

        // for attachment/s
        $this->saveAttachments($data, $guideline);

        $guideline->save(); //save data

        // for post topics
        $this->attachTopics($data, $guideline);



        return $guideline;
    }

    public function saveBanner($data, $guideline)
    {
        if($data->banner){
            $filename = $data->banner->getClientOriginalName();
            $file_path = config('app.url', 'http://localhost:8081/') . 'storage/' . $data->banner->store('/banner/guideline', 'public');
            $guideline->banner_name = $filename;
            $guideline->banner_path = $file_path;
        }
    }

    public function attachType($data, $guideline)
    {
        // $guideline->type()->attach($data->post_type);
        $guideline->type_id = $data->post_type;
    }

    public function attachTopics($data, $guideline)
    {
        // for post topic
        if($data->topics){
                $guideline->topics()->sync($data->topics);
        }
    }

    public function saveAttachments($data, $guideline)
    {
        if($data->attachment){
            $filename = $data->attachment->getClientOriginalName();
            $attachment_path = config('app.url', 'http://localhost:8081/') . 'storage/' . $data->attachment->store('/attachment/guideline', 'public');
            $guideline->attachment_name = $filename;
            $guideline->attachment_path = $attachment_path;


            // foreach ($data->attachment as $attachment) {
            //     $attachment_save = new Attachment;

            //     $filename = $attachment->getClientOriginalName();
            //     $file_path = $attachment->store('attachment/guideline', 'public');

            //     $attachment_save->name = $filename;
            //     $attachment_save->path = $file_path; //attachment
            //     $attachment_save->guideline_id = $guideline->id;
            //     $attachment_save->save();
            // }
        }
    }
}
