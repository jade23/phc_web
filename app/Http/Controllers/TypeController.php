<?php

namespace App\Http\Controllers;

use App\Models\Type;
use Illuminate\Http\Request;
use Log;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;

        $validate = $this->validate($request, [
            'type_name' => ['required', 'string', 'max:20', 'unique:types,name'],
        ]);

        try {

            $type = new Type;
            $type->name = $request->type_name;
            $type->save();

            return redirect()->back()->with('success', 'Successfully added!');

        } catch (\Throwable $th) {
            Log::info("Error while saving type for guideline. Error Message : " . $th);
            return redirect()->back()->with('error', 'Opss! Something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::findOrFail($id)->delete();

        if ($type) {
            return response()->json(['status' => 'success', 'message' => 'Type was successfully deleted.'], 200);
        }

        return response()->json(['status' => 'error', 'message' => 'Type was not able to remove.'], 200);
    }
}
