<?php

namespace App\Http\Controllers;

use App\Models\MedicalIntake;
use Illuminate\Http\Request;

class MedicalIntakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MedicalIntake  $medicalIntake
     * @return \Illuminate\Http\Response
     */
    public function show(MedicalIntake $medicalIntake)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MedicalIntake  $medicalIntake
     * @return \Illuminate\Http\Response
     */
    public function edit(MedicalIntake $medicalIntake)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MedicalIntake  $medicalIntake
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MedicalIntake $medicalIntake)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MedicalIntake  $medicalIntake
     * @return \Illuminate\Http\Response
     */
    public function destroy(MedicalIntake $medicalIntake)
    {
        //
    }
}
