<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('topics')->delete();

        \DB::table('topics')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Events',
                'created_at' => '2021-09-21 22:25:27',
                'updated_at' => '2021-09-21 22:25:27',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'Tips',
                'created_at' => '2021-09-21 22:25:31',
                'updated_at' => '2021-09-21 22:25:31',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'Announcements',
                'created_at' => '2021-09-21 22:25:38',
                'updated_at' => '2021-09-21 22:25:38',
            ),
        ));


    }
}
