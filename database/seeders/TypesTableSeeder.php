<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('types')->delete();
        
        \DB::table('types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Regular Post',
                'created_at' => '2021-09-21 22:25:47',
                'updated_at' => '2021-09-21 22:25:47',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'With Attachment',
                'created_at' => '2021-09-21 22:26:20',
                'updated_at' => '2021-09-21 22:26:20',
            ),
        ));
        
        
    }
}