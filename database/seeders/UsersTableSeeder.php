<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$81uSJ/fP.mKqU/1lgfVyaeLPDJ/bfkxbkG0sxI.RQdK4VsidmCN0m', //password
                'remember_token' => NULL,
                'is_backend_user' => 1,
                'is_super_admin' => 1,
                'created_at' => '2021-09-06 09:58:47',
                'updated_at' => '2021-09-06 09:58:47',
                'back_end_contact' => '09761097352',
            ),
        ));


    }
}
