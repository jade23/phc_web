<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->date('birth_date')->nullable();
            $table->text('address')->nullable();
            $table->string('contact_number')->nullable();
            $table->string('husband_name')->nullable();
            $table->string('husband_contact_number')->nullable();
            $table->date('check_up')->nullable();
            $table->boolean('is_pregnant')->nullable()->default(false);
            $table->boolean('status')->default(true);
            $table->text('profile_picture')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
