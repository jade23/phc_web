<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuidelineTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guideline_topic', function (Blueprint $table) {
            $table->unsignedBigInteger('guideline_id')->index();
            $table->foreign('guideline_id')->references('id')->on('guidelines')->onDelete('cascade');;
            $table->unsignedBigInteger('topic_id')->index();
            $table->foreign('topic_id')->references('id')->on('topics')->onDelete('cascade');;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guideline_topic');
    }
}
